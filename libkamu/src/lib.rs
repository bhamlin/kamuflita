pub mod data;
pub mod error;
pub mod key;
pub mod otp;
// pub mod tools;

pub trait IsKey {
    fn get_key(&self) -> &Vec<u8>;
}
