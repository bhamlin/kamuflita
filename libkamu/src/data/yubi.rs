#![allow(unused)]

use itertools::Itertools;

const MODHEX_INTO_ALPHABET: &[u8] = b"cbdefghijklnrtuv";
const MODHEX_FROM_ALPHABET: &[u8] = b"0123456789abcdef";

pub fn into_modhex(input: String) -> Option<String> {
    modhex_convert(input, MODHEX_FROM_ALPHABET, MODHEX_INTO_ALPHABET)
}

pub fn from_modhex(input: String) -> Option<String> {
    modhex_convert(input, MODHEX_INTO_ALPHABET, MODHEX_FROM_ALPHABET)
}

fn modhex_convert(input: String, alpha_from: &[u8], alpha_into: &[u8]) -> Option<String> {
    String::from_utf8(
        input
            .as_bytes()
            .iter()
            .filter_map(|&char| Some(alpha_into[alpha_from.iter().position(|&c| c == char)?]))
            .collect_vec(),
    )
    .ok()
}
