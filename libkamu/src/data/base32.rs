pub const RFC4648_ALPHABET: &[u8; 32] = b"ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
pub const RFC4648_INV_ALPHABET: [u8; 80] = [
    0xff, 0xff, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, //
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, //
    0xff, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, //
    0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, //
    0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, //
    0x17, 0x18, 0x19, 0xff, 0xff, 0xff, 0xff, 0xff, //
    0xff, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, //
    0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, //
    0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, //
    0x17, 0x18, 0x19, 0xff, 0xff, 0xff, 0xff, 0xff, //
];

pub fn decode(inv_alphabet: [u8; 80], data: &[u8]) -> Option<Vec<u8>> {
    let mut stream = data
        .iter()
        .filter(|value| **value >= b'0')
        .map(|byte| *byte - b'0')
        .filter(|index| *index < inv_alphabet.len() as u8)
        .map(|code_point| inv_alphabet[code_point as usize])
        .filter(|bits| *bits != 0xff)
        .peekable();

    let mut result = Vec::<u8>::new();
    while stream.peek().is_some() {
        let chunk: Vec<u8> = stream.by_ref().take(8).collect();

        // Output byte 1
        if chunk.len() >= 2 {
            let mut value: u8 = chunk[0] << 3;
            value += chunk[1] >> 2;
            result.push(value);
        }
        // Output byte 2
        if chunk.len() >= 4 {
            let mut value: u8 = chunk[1] << 6;
            value += chunk[2] << 1;
            value += chunk[3] >> 4;
            result.push(value);
        }
        // Output byte 3
        if chunk.len() >= 5 {
            let mut value: u8 = chunk[3] << 4;
            value += chunk[4] >> 1;
            result.push(value);
        }
        // Output byte 4
        if chunk.len() >= 7 {
            let mut value: u8 = chunk[4] << 7;
            value += chunk[5] << 2;
            value += chunk[6] >> 3;
            result.push(value);
        }
        // Output byte 8
        if chunk.len() >= 8 {
            let mut value: u8 = chunk[6] << 5;
            value += chunk[7];
            result.push(value);
        }
    }

    Some(result)
}
