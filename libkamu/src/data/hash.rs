use digest::Mac;
use error_stack::{IntoReport, Report, ResultExt};
use hmac::Hmac;
use sha1::Sha1;
use sha2::{Sha256, Sha512};

use crate::error::KamuflitaError;

pub trait Hasher {
    fn hash(&self, key: &[u8], time: &[u8]) -> Result<Vec<u8>, Report<KamuflitaError>>;
}

pub enum HashFunction {
    Sha1,
    Sha256,
    Sha512,
}

impl Hasher for HashFunction {
    fn hash(&self, key: &[u8], time: &[u8]) -> Result<Vec<u8>, Report<KamuflitaError>> {
        match self {
            Self::Sha1 => (SHA1 {}).hash(key, time),
            Self::Sha256 => (SHA256 {}).hash(key, time),
            Self::Sha512 => (SHA512 {}).hash(key, time),
        }
    }
}

struct SHA1;
impl Hasher for SHA1 {
    fn hash(&self, key: &[u8], time: &[u8]) -> Result<Vec<u8>, Report<KamuflitaError>> {
        let mut h = Hmac::<Sha1>::new_from_slice(key)
            .into_report()
            .change_context(KamuflitaError::DigestStartupFailure)?;
        h.update(time);
        let output = h.finalize().into_bytes();
        Ok(output.to_vec())
    }
}

struct SHA256;
impl Hasher for SHA256 {
    fn hash(&self, key: &[u8], time: &[u8]) -> Result<Vec<u8>, Report<KamuflitaError>> {
        let mut h = Hmac::<Sha256>::new_from_slice(key)
            .into_report()
            .change_context(KamuflitaError::DigestStartupFailure)?;
        h.update(time);
        let output = h.finalize().into_bytes();
        Ok(output.to_vec())
    }
}
struct SHA512;
impl Hasher for SHA512 {
    fn hash(&self, key: &[u8], time: &[u8]) -> Result<Vec<u8>, Report<KamuflitaError>> {
        let mut h = Hmac::<Sha512>::new_from_slice(key)
            .into_report()
            .change_context(KamuflitaError::DigestStartupFailure)?;
        h.update(time);
        let output = h.finalize().into_bytes();
        Ok(output.to_vec())
    }
}
