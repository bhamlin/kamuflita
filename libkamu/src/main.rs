#![allow(unused)]

use aes::cipher::{generic_array::GenericArray, BlockCipher, BlockDecrypt, BlockEncrypt, KeyInit};
use aes::Aes128;
use libkamu::data::hex;
use libkamu::data::yubi::into_modhex;

fn main() {
    // let key = GenericArray::from([0u8; 16]);
    // let mut block = GenericArray::from([42u8; 16]);

    // // Initialize cipher
    // let cipher = Aes128::new(&key);

    // let block_copy = block;

    // // Encrypt block in-place
    // cipher.encrypt_block(&mut block);

    // // And decrypt it back
    // cipher.decrypt_block(&mut block);
    // assert_eq!(block, block_copy);

    // // Implementation supports parallel block processing. Number of blocks
    // // processed in parallel depends in general on hardware capabilities.
    // // This is achieved by instruction-level parallelism (ILP) on a single
    // // CPU core, which is differen from multi-threaded parallelism.
    // let mut blocks = [block; 100];
    // cipher.encrypt_blocks(&mut blocks);

    // for block in blocks.iter_mut() {
    //     cipher.decrypt_block(block);
    //     assert_eq!(block, &block_copy);
    // }

    // // `decrypt_blocks` also supports parallel block processing.
    // cipher.decrypt_blocks(&mut blocks);

    // for block in blocks.iter_mut() {
    //     cipher.encrypt_block(block);
    //     assert_eq!(block, &block_copy);
    // }
    let key = GenericArray::from([
        0xd6, 0xf4, 0x21, 0xed, 0x20, 0xea, 0x0e, 0x7d, //
        0x48, 0x34, 0x84, 0xa1, 0x92, 0xa5, 0x91, 0xfc, //
    ]);
    let mut block = GenericArray::from([
        0x2d, 0x0f, 0xb5, 0x23, 0x57, 0x3b, 0x05, 0xe3, //
        0x95, 0xde, 0xd1, 0xa3, 0x8c, 0x01, 0x29, 0x8b, //
    ]);

    let cipher = Aes128::new(&key);
    cipher.decrypt_block(&mut block);

    println!("{}", hex(block.as_slice()));
}

fn hex_string_to_u8_array() -> Vec<u8> {
    let mut vec = vec![];

    vec
}
