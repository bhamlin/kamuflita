use super::{
    dynamic_truncation, dynamic_truncation_raw, get_last_n, OneTimePassword,
    OneTimePasswordGenerator,
};
use crate::{
    data::hash::{HashFunction, Hasher},
    error::KamuflitaError,
    key::OtpKey,
    IsKey,
};
use chrono::Utc;
use error_stack::Result;

pub struct TimeBasedState;

impl TimeBasedState {
    fn get_time() -> u64 {
        Utc::now().timestamp() as u64
    }
}

pub struct TOTPResult {
    pub otp: String,
    pub life: u8,
}

pub struct TimeBasedOtp {
    key: OtpKey,
    hasher: HashFunction,
    output_length: usize,
    window: usize,
}

impl TimeBasedOtp {
    pub fn get_one(
        key: &OtpKey,
        hasher: HashFunction,
        length: usize,
        window: usize,
    ) -> Result<OneTimePassword, KamuflitaError> {
        Self::get_one_at_time(key, hasher, length, window, TimeBasedState::get_time())
    }

    pub fn get_one_string(
        key: String,
        hasher: HashFunction,
        length: usize,
        window: usize,
    ) -> Result<OneTimePassword, KamuflitaError> {
        Self::get_one_at_time(
            &OtpKey::from_string(key)?,
            hasher,
            length,
            window,
            TimeBasedState::get_time(),
        )
    }

    pub fn get_one_at_time(
        key: &OtpKey,
        hasher: HashFunction,
        length: usize,
        window: usize,
        at_time: u64,
    ) -> Result<OneTimePassword, KamuflitaError> {
        let totp = TimeBasedOtp::new_using_hash_window(key, hasher, length, window);
        totp.generate_with_state(at_time)
    }
}

impl OneTimePasswordGenerator<u64> for TimeBasedOtp {
    fn new(key: &OtpKey, output_length: usize) -> Self {
        Self::new_using_hash_window(key, HashFunction::Sha1, output_length, 30)
    }

    fn new_using_hash(key: &OtpKey, hasher: HashFunction, output_length: usize) -> Self {
        Self::new_using_hash_window(key, hasher, output_length, 30)
    }

    fn new_using_hash_window(
        key: &OtpKey,
        hasher: HashFunction,
        output_length: usize,
        window: usize,
    ) -> Self {
        Self {
            key: OtpKey::from_array(key.get_key()),
            hasher,
            output_length,
            window,
        }
    }

    fn generate(&self) -> Result<OneTimePassword, KamuflitaError> {
        self.generate_with_state(TimeBasedState::get_time())
    }

    fn generate_with_state(&self, time: u64) -> Result<OneTimePassword, KamuflitaError> {
        let window = self.window as u64;
        let life = time % window;
        let life = window - life;
        let now = u64::to_be_bytes(time / window);

        let value = self.hasher.hash(self.key.get_key(), &now)?;
        let (value, bytes) = dynamic_truncation_raw(&value)?;

        let otp = value % 10_u32.pow(self.output_length as u32);
        let otp = format!("{otp:016}");
        let otp = get_last_n(otp, self.output_length);

        Ok(OneTimePassword {
            data: bytes.to_vec(),
            value,
            string: otp,
            time_to_live: life,
        })
    }
}

#[allow(unused)]
pub fn generate<F>(hasher: F, key: &[u8], time: u64, step: u8, length: u8) -> Option<TOTPResult>
where
    F: Fn(&[u8], &[u8]) -> Vec<u8>,
{
    let life = time % 30;
    let life = 30 - (life as u8);
    let now = u64::to_be_bytes(time / 30);

    // let mut hasher = H::new();
    // hasher.update(key);
    // let mut token = Hmac::<H>::new_from_slice(key).ok()?;
    // hasher.update(&now);
    let otp = dynamic_truncation(&hasher(key, &now)).ok()?;

    let otp = otp % 10_u32.pow(length as u32);
    let otp = format!("{otp:016}");
    let otp: String = get_last_n(otp, length.into());

    Some(TOTPResult { otp, life })
}
