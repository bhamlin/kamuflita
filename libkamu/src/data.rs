pub mod aes;
pub mod base32;
pub mod hash;
pub mod yubi;

use std::fmt::Write;

pub fn hex(xs: &[u8]) -> String {
    // xs.iter().map(|x| format!("{:02x}", x)).collect()
    xs.iter().fold(String::new(), |mut output, b| {
        write!(output, "{b:02X}").ok();
        output
    })
}

pub fn ascii(xs: &[u8]) -> String {
    xs.iter()
        .map(|x| {
            if *x < b' ' || *x > b'~' {
                '.'
            } else {
                char::from(*x)
            }
        })
        .collect::<String>()
}
