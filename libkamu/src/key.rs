use std::fmt::Display;

use crate::{data::base32, error::KamuflitaError, IsKey};

pub struct OtpKey {
    key: Vec<u8>,
}

impl Display for OtpKey {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let items: Vec<String> = self.get_key().iter().map(|e| format!("{e:02x}")).collect();
        write!(f, "{}", items.join(""))
    }
}

impl OtpKey {
    pub fn from_string<Stringlike>(data: Stringlike) -> Result<OtpKey, KamuflitaError>
    where
        Stringlike: Into<String>,
    {
        let data: String = data.into();
        let key = base32::decode(base32::RFC4648_INV_ALPHABET, data.as_bytes())
            .ok_or(KamuflitaError::Base32Decode)?;
        Ok(Self::from_array(&key))
    }

    pub fn from_array(key: &[u8]) -> OtpKey {
        Self { key: key.to_vec() }
    }
}

impl IsKey for OtpKey {
    fn get_key(&self) -> &Vec<u8> {
        &self.key
    }
}
