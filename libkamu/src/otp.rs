pub mod totp;
pub mod yubi;

use crate::{data::hash::HashFunction, error::KamuflitaError, key::OtpKey};
use error_stack::{IntoReport, Result, ResultExt};

pub const DEFAULT_SLICE_SIZE: usize = 4;
pub const DEFAULT_OUTPUT_MASK: u32 = 0x7fff_ffff;

pub trait OneTimePasswordGenerator<State> {
    /// Build new OTP generator with the default hash algorithm
    fn new(key: &OtpKey, output_length: usize) -> Self;

    /// Build new OTP generator with the specified hash algorithm
    fn new_using_hash(key: &OtpKey, hasher: HashFunction, output_length: usize) -> Self;

    /// Build new OTP generator with the specified hash algorithm and window
    fn new_using_hash_window(
        key: &OtpKey,
        hasher: HashFunction,
        output_length: usize,
        window: usize,
    ) -> Self;

    /// Generate a OneTimePassword output with the default state
    fn generate(&self) -> Result<OneTimePassword, KamuflitaError>;

    /// Generate a OneTimePassword output with the specified state
    fn generate_with_state(&self, state: State) -> Result<OneTimePassword, KamuflitaError>;
}

pub struct OneTimePassword {
    pub data: Vec<u8>,
    pub value: u32,
    pub string: String,
    pub time_to_live: u64,
}

/// Convenience function for many OTP types
fn get_last_n(input: String, count: usize) -> String {
    input
        .chars()
        .rev()
        .take(count)
        .collect::<String>()
        .chars()
        .rev()
        .collect()
}

pub fn dynamic_truncation_raw(input: &[u8]) -> Result<(u32, [u8; 4]), KamuflitaError> {
    let length = input.len();
    let index = input[length - 1] & 0xf;
    let idx = index as usize;
    let idn = idx + DEFAULT_SLICE_SIZE;
    let slice: [u8; 4] = input[idx..idn]
        .try_into()
        .into_report()
        .change_context(KamuflitaError::CastToByteArray)?;
    let value_raw = u32::from_be_bytes(slice);
    let value = value_raw & DEFAULT_OUTPUT_MASK;

    Ok((value, u32::to_be_bytes(value)))
}

pub fn dynamic_truncation(input: &[u8]) -> Result<u32, KamuflitaError> {
    dynamic_truncation_size_mask(input, DEFAULT_SLICE_SIZE, DEFAULT_OUTPUT_MASK)
}

pub fn dynamic_truncation_size(input: &[u8], slice_size: usize) -> Result<u32, KamuflitaError> {
    dynamic_truncation_size_mask(input, slice_size, DEFAULT_OUTPUT_MASK)
}

pub fn dynamic_truncation_size_mask(
    input: &[u8],
    slice_size: usize,
    output_mask: u32,
) -> Result<u32, KamuflitaError> {
    let length = input.len();
    let index = input[length - 1] & 0xf;
    let idx = index as usize;
    let idn = idx + slice_size;
    let slice: [u8; 4] = input[idx..idn]
        .try_into()
        .into_report()
        .change_context(KamuflitaError::CastToByteArray)?;
    let value_raw = u32::from_be_bytes(slice);
    let value = value_raw & output_mask;

    Ok(value)
}
