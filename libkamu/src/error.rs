use error_stack::Context;
use std::fmt::Display;

#[derive(Debug)]
pub enum KamuflitaError {
    Base32Decode,
    CastToByteArray,
    DigestStartupFailure,
}

impl Context for KamuflitaError {}
impl Display for KamuflitaError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            Self::Base32Decode => "Error decoding base32 input",
            Self::CastToByteArray => "Error casting to byte array",
            Self::DigestStartupFailure => "Error starting digest engine",
        };
        write!(f, "{message}")
    }
}
