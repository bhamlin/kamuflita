#[allow(unused)]
#[cfg(test)]
mod yubikey {
    use std::vec;

    use libkamu::data::yubi::into_modhex;

    struct TV {
        pub input: String,
        pub expected: String,
    }

    impl TV {
        pub fn from(input: &str, expected: &str) -> Self {
            Self {
                input: input.to_string(),
                expected: expected.to_string(),
            }
        }
    }

    #[test]
    fn test_modhex() {
        let test_vectors = vec![
            TV::from("000000000000", "cccccccccccc"),
            TV::from("000000000001", "cccccccccccb"),
            TV::from("000000000020", "ccccccccccdc"),
            TV::from("000000000300", "cccccccccecc"),
            TV::from("000000004000", "ccccccccfccc"),
            TV::from("000000050000", "cccccccgcccc"),
            TV::from("000000600000", "cccccchccccc"),
            TV::from("000007000000", "cccccicccccc"),
            TV::from("000080000000", "ccccjccccccc"),
            TV::from("000900000000", "ccckcccccccc"),
            TV::from("00a000000000", "cclccccccccc"),
            TV::from("0b0000000000", "cncccccccccc"),
            TV::from("c00000000000", "rccccccccccc"),
            TV::from("0d0000000000", "ctcccccccccc"),
            TV::from("00e000000000", "ccuccccccccc"),
            TV::from("000f00000000", "cccvcccccccc"),
            TV::from("3890766c0e98", "ejkcihhrcukj"),
            TV::from("1948ca3ae920", "bkfjrlelukdc"),
            TV::from("5463a5d518da", "gfhelgtgbjtl"),
            TV::from("9dd44d954cf5", "kttfftkgfrvg"),
            TV::from("0c5844bef3de", "crgjffnuvetu"),
            TV::from("8a154ebd0335", "jlbgfuntceeg"),
            TV::from("dd16d6ef03c8", "ttbhthuvcerj"),
            TV::from("f1f5e8780445", "vbvgujijcffg"),
            TV::from("d6e5d479a9bc", "thugtfiklknr"),
            TV::from("4373663b95cf", "feiehhenkgrv"),
        ];

        for vector in test_vectors {
            let actual = into_modhex(vector.input).unwrap();
            assert_eq!(vector.expected, actual);
        }
    }

    fn test_yubi_code() {
        let public_id = "vvccccfhjtic";
        let private_id = "d4e93e1f5e0d";
        let secret_key = "d6f421ed20ea0e7d483484a192a591fc";

        let test_vectors = vec![
            TV::from(
                "vvccccfhjticdtcvngdegiencguekgtutblejrcbdkjn",
                "cccccccccccc",
            ),
            TV::from(
                "vvccccfhjticnjkhkhbhtvletnglhjjtngrdklhdvhrr",
                "cccccccccccc",
            ),
            TV::from(
                "vvccccfhjticlinkhkjictvjkdenejlerbknflbnnegf",
                "cccccccccccc",
            ),
            TV::from(
                "vvccccfhjticfrcetrdrddjchtljejnehgfchdbltntl",
                "cccccccccccc",
            ),
            TV::from(
                "vvccccfhjticnnhcbnulbvefeknvnbendtteknfvhtrf",
                "cccccccccccc",
            ),
        ];
    }
}
