// let key = b32decode_rfc4648(b"I24BWHRM2BDOLFZV").unwrap();
// let token = totp_sha1(&key).unwrap();
// println!("{} ({})", token.otp, token.life);

// #[cfg(test)]
mod totp {
    use libkamu::{
        key::OtpKey,
        otp::{totp::TimeBasedOtp, OneTimePasswordGenerator},
        IsKey,
    };

    #[test]
    fn test_totp_sha1() {
        let key = "gezdgnbvgy3tqojqgezdgnbvgy3tqojq";
        let test_key = OtpKey::from_string(key).unwrap();
        assert_eq!(20, test_key.get_key().len());

        let totp = TimeBasedOtp::new(&test_key, 8);

        let res = totp.generate_with_state(59u64).unwrap();
        assert_eq!("94287082", res.string);

        let res = totp.generate_with_state(1111111109u64).unwrap();
        assert_eq!("07081804", res.string);

        let res = totp.generate_with_state(1111111111u64).unwrap();
        assert_eq!("14050471", res.string);

        let res = totp.generate_with_state(1234567890u64).unwrap();
        assert_eq!("89005924", res.string);

        let res = totp.generate_with_state(2000000000u64).unwrap();
        assert_eq!("69279037", res.string);

        let res = totp.generate_with_state(20000000000u64).unwrap();
        assert_eq!("65353130", res.string);
    }

    #[test]
    fn test_totp_sha2_256() {
        let key = "gezdgnbvgy3tqojqgezdgnbvgy3tqojqgezdgnbvgy3tqojqgeza====";
        let test_key = OtpKey::from_string(key).unwrap();
        assert_eq!(32, test_key.get_key().len());

        let totp =
            TimeBasedOtp::new_using_hash(&test_key, libkamu::data::hash::HashFunction::Sha256, 8);

        let res = totp.generate_with_state(59u64).unwrap();
        assert_eq!("46119246", res.string);

        let res = totp.generate_with_state(1111111109u64).unwrap();
        assert_eq!("68084774", res.string);

        let res = totp.generate_with_state(1111111111u64).unwrap();
        assert_eq!("67062674", res.string);

        let res = totp.generate_with_state(1234567890u64).unwrap();
        assert_eq!("91819424", res.string);

        let res = totp.generate_with_state(2000000000u64).unwrap();
        assert_eq!("90698825", res.string);

        let res = totp.generate_with_state(20000000000u64).unwrap();
        assert_eq!("77737706", res.string);
    }

    #[test]
    fn test_totp_sha2_512() {
        let key = "GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQGEZDGNA";
        let test_key = OtpKey::from_string(key).unwrap();
        assert_eq!(64, test_key.get_key().len());

        let totp =
            TimeBasedOtp::new_using_hash(&test_key, libkamu::data::hash::HashFunction::Sha512, 8);

        let res = totp.generate_with_state(59u64).unwrap();
        assert_eq!("90693936", res.string);

        let res = totp.generate_with_state(1111111109u64).unwrap();
        assert_eq!("25091201", res.string);

        let res = totp.generate_with_state(1111111111u64).unwrap();
        assert_eq!("99943326", res.string);

        let res = totp.generate_with_state(1234567890u64).unwrap();
        assert_eq!("93441116", res.string);

        let res = totp.generate_with_state(2000000000u64).unwrap();
        assert_eq!("38618901", res.string);

        let res = totp.generate_with_state(20000000000u64).unwrap();
        assert_eq!("47863826", res.string);
    }
}
