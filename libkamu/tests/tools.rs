#[cfg(test)]
mod dyn_trunc {
    use libkamu::otp::dynamic_truncation;

    #[test]
    fn test_dynamic_truncation_1() {
        let input = &[
            0x1f, 0x86, 0x98, 0x69, 0x0e, //
            0x02, 0xca, 0x16, 0x61, 0x85, //
            0xd0, 0xef, 0x7f, 0x19, 0xda, //
            0x8e, 0x94, 0x5b, 0x55, 0x5a, //
        ];

        let result = dynamic_truncation(input).unwrap();
        assert_eq!(1357872921, result);
    }
}
