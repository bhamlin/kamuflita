use clap::{Args, Subcommand};

#[derive(Debug, Args)]
pub struct TokenStoreArgs {
    #[clap(subcommand)]
    pub store_action: StoreAction,
}

#[derive(Debug, Subcommand)]
pub enum StoreAction {
    /// Create an empty token store
    #[clap(name = "init")]
    Initialize(StoreInitOptions),
}

#[derive(Debug, Args)]
pub struct StoreInitOptions {}
