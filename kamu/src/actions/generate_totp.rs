use clap::{Args, ValueEnum};
use libkamu::data::hash::HashFunction;

#[derive(Debug, Args)]
pub struct GenerateTotpArgs {
    /// TOTP key in base32
    pub key: String,

    #[clap(short = 't', long = "at-time", name = "time-in-seconds")]
    /// Time in seconds to use for generation [default: now]
    pub at_time: Option<u64>,

    #[clap(short = 'l', long = "length", name = "char-count", default_value_t = 6)]
    /// Output character length
    pub output_length: usize,

    #[clap(
        short = 'w',
        long = "window",
        name = "size-in-seconds",
        default_value_t = 30
    )]
    /// Time quantization window in seconds
    pub window: usize,

    #[clap(value_enum, long = "hash-method", name = "hash-method", default_value_t = GenerateTotpTypeList::Sha1)]
    /// Hash method for TOTP generation
    pub hash_method: GenerateTotpTypeList,
}

#[derive(Debug, Clone, ValueEnum)]
pub enum GenerateTotpTypeList {
    Sha1,
    Sha256,
    Sha512,
}

impl From<GenerateTotpTypeList> for HashFunction {
    fn from(val: GenerateTotpTypeList) -> Self {
        match val {
            GenerateTotpTypeList::Sha1 => HashFunction::Sha1,
            GenerateTotpTypeList::Sha256 => HashFunction::Sha256,
            GenerateTotpTypeList::Sha512 => HashFunction::Sha512,
        }
    }
}
