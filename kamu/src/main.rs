// #![allow(unused)]
pub mod actions;
pub mod cli_arg;

use crate::cli_arg::CliAction;
use clap::Parser;
use cli_arg::CliOptions;
use env_logger::{Builder, Env};
use error_stack::Result;
use libkamu::{data::hex, error::KamuflitaError, key::OtpKey, otp::totp::TimeBasedOtp};
use log::trace;

fn main() -> Result<(), KamuflitaError> {
    let options = init();

    #[allow(unreachable_patterns)]
    match &options.action {
        CliAction::GenerateProvidedTotp(_) => generate_an_otp(&options),
        CliAction::ManageStore(_) => manage_store(&options),
        _ => Ok(()),
    }
}

fn init() -> CliOptions {
    dotenv::dotenv().ok();
    let build_id = buildid::build_id().unwrap();
    let options = cli_arg::CliOptions::parse();
    Builder::from_env(Env::default().default_filter_or(&options.rust_log)).init();
    trace!("Build: {}", hex(build_id));

    options
}

/// Generate a One Time Password from a supplied input key
fn generate_an_otp(options: &CliOptions) -> Result<(), KamuflitaError> {
    #[allow(unreachable_patterns)]
    let otp = match &(options.action) {
        CliAction::GenerateProvidedTotp(totp_options) => TimeBasedOtp::get_one(
            &OtpKey::from_string(totp_options.key.clone())?,
            totp_options.hash_method.clone().into(),
            totp_options.output_length,
            totp_options.window,
        ),
        _ => todo!(),
    }?;

    Ok(println!("{} [{}]", otp.string, otp.time_to_live))
}

/// Manage a token store file
fn manage_store(_options: &CliOptions) -> Result<(), KamuflitaError> {
    Ok(())
}
