use crate::actions::{generate_totp::GenerateTotpArgs, token_store::TokenStoreArgs};
use clap::{Parser, Subcommand};
use std::path::PathBuf;

#[derive(Debug, Parser)]
pub struct CliOptions {
    /// Log level
    #[clap(env)]
    pub rust_log: String,

    /// Store location
    #[clap(
        short = 's',
        long = "store",
        env = "KAMU_STORE",
        default_value = "~/.kamu/default/token.store"
    )]
    pub store_location: PathBuf,

    #[clap(subcommand)]
    pub action: CliAction,

    misc: Option<String>,
}

#[derive(Debug, Subcommand)]
pub enum CliAction {
    /// Generate a TOTP a la carte from a provided key
    #[clap(name = "generate-once")]
    GenerateProvidedTotp(GenerateTotpArgs),

    /// Manage a kamuflita token store
    #[clap(name = "store")]
    ManageStore(TokenStoreArgs),
}
